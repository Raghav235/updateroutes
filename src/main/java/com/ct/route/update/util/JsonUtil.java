package com.ct.route.update.util;

import com.google.common.base.Charsets;
import com.google.common.io.CharStreams;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.ct.route.update.config.Config;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;

public class JsonUtil {

    public static String objectToJson(Object javaObject) {
        Gson gson = new GsonBuilder().create();
        String json = gson.toJson(javaObject);
        return json;
    }

    public static String makePostCall(String body, String targetURL, Config configuration) throws IOException {
        String query_url = targetURL;
        String json = body;

        String result = null;
        try {
            URL url = new URL(query_url);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Cookie", configuration.getCookie());
            conn.setRequestProperty("authorization",configuration.getAuthorization());
            conn.setDoOutput(true);
            //com.ct.route.update.conn.setDoInput(true);
            conn.setRequestMethod("POST");


            OutputStream os = conn.getOutputStream();
            os.write(json.getBytes("UTF-8"));
            os.close();
            // read the response
            InputStream in = new BufferedInputStream(conn.getInputStream());
            result = CharStreams.toString(new InputStreamReader(in, Charsets.UTF_8));
            System.out.println("Response:"+result);

            conn.disconnect();
        } catch (Exception e) {
            System.out.println(e);
        }
        return result;
    }


}
