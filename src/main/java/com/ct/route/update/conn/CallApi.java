package com.ct.route.update.conn;

import com.ct.route.update.config.Config;
import com.ct.route.update.util.JsonUtil;

import java.io.IOException;

public class CallApi {
    public static void callApi(String from, String to, String cmd, Config configuration,boolean isIntl) throws IOException {

        Body body = new Body();
        body.setFromCode(from);
        body.setToCode(to);
        String[] airline = new String[1];
        String empty[] = new String[0];

        airline[0] = configuration.getAirline();
        if (cmd.equalsIgnoreCase("add")) {
            body.setAddedAirlines(airline);
            body.setRemovedAirlines(empty);
        } else if (cmd.equalsIgnoreCase("delete")) {
            body.setRemovedAirlines(airline);
            body.setAddedAirlines(empty);
        }
        body.setEMAIL_ID(configuration.getEmailid());
        body.setUSER_ID(configuration.getUserid());
        String bodyParam = JsonUtil.objectToJson(body);
        String url;
        if(isIntl){
            url = configuration.getIntlUrl();
        }
        else{
            url = configuration.getDomesticUrl();
        }
        JsonUtil.makePostCall(bodyParam, url, configuration);

    }
}
