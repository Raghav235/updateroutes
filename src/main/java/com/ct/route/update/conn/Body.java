package com.ct.route.update.conn;

public class Body
{
    private String[] addedAirlines;

    private String[] removedAirlines;

    private String USER_ID;

    private String toCode;

    private String EMAIL_ID;

    private String fromCode;

    public String[] getAddedAirlines ()
    {
        return addedAirlines;
    }

    public void setAddedAirlines (String[] addedAirlines)
    {
        this.addedAirlines = addedAirlines;
    }

    public String[] getRemovedAirlines ()
    {
        return removedAirlines;
    }

    public void setRemovedAirlines (String[] removedAirlines)
    {
        this.removedAirlines = removedAirlines;
    }

    public String getUSER_ID ()
    {
        return USER_ID;
    }

    public void setUSER_ID (String USER_ID)
    {
        this.USER_ID = USER_ID;
    }

    public String getToCode ()
    {
        return toCode;
    }

    public void setToCode (String toCode)
    {
        this.toCode = toCode;
    }

    public String getEMAIL_ID ()
    {
        return EMAIL_ID;
    }

    public void setEMAIL_ID (String EMAIL_ID)
    {
        this.EMAIL_ID = EMAIL_ID;
    }

    public String getFromCode ()
    {
        return fromCode;
    }

    public void setFromCode (String fromCode)
    {
        this.fromCode = fromCode;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [addedAirlines = "+addedAirlines+", removedAirlines = "+removedAirlines+", USER_ID = "+USER_ID+", toCode = "+toCode+", EMAIL_ID = "+EMAIL_ID+", fromCode = "+fromCode+"]";
    }
}