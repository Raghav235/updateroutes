package com.ct.route.update.conn;

import com.ct.route.update.config.Config;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import java.io.File;
import java.io.IOException;

public class GetConnection {

    public static Workbook getConnection(Config config) throws IOException, InvalidFormatException {
        Workbook workbook = WorkbookFactory.create(new File(config.getRoute_file()));
        return workbook;
    }
}
