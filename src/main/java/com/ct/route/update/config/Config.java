package com.ct.route.update.config;

public class Config {
    String userid;
    String emailid;
    String intlUrl;
    String cookie;
    String route_file;
    String authorization;
    String noRefresh;
    String domesticUrl;
    String dom_airport_file;
    String airline;

    public String getAirline() {
        return airline;
    }

    public void setAirline(String airline) {
        this.airline = airline;
    }

    public String getDom_airport_file() {
        return dom_airport_file;
    }

    public void setDom_airport_file(String dom_airport_file) {
        this.dom_airport_file = dom_airport_file;
    }

    public String getDomesticUrl() {
        return domesticUrl;
    }

    public void setDomesticUrl(String domesticUrl) {
        this.domesticUrl = domesticUrl;
    }

    public void Config() {
        this.noRefresh = "false";
    }

    public String getNoRefresh() {
        return noRefresh;
    }

    public void setNoRefresh(String noRefresh) {
        this.noRefresh = noRefresh;
    }

    public String getAuthorization() {
        return authorization;
    }

    public void setAuthorization(String authorization) {
        this.authorization = authorization;
    }

    public String getRoute_file() {
        return route_file;
    }

    public void setRoute_file(String route_file) {
        this.route_file = route_file;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getEmailid() {
        return emailid;
    }

    public void setEmailid(String emailid) {
        this.emailid = emailid;
    }

    public String getIntlUrl() {
        return intlUrl;
    }

    public void setIntlUrl(String intlUrl) {
        this.intlUrl = intlUrl;
    }

    public String getCookie() {
        return cookie;
    }

    public void setCookie(String cookie) {
        this.cookie = cookie;
    }
}
