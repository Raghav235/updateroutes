package com.ct.route.update.impl;

import com.ct.route.update.config.Config;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.*;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

public class DomesticRoutesMap {

    public static Map<String, Boolean> getDomesticFlightsMap(Config configuration) throws IOException, InvalidFormatException {
        Workbook workbook;
        workbook = WorkbookFactory.create(new File(configuration.getDom_airport_file()));
        Sheet ds = workbook.getSheetAt(0);


        Map<String,Boolean> map = new LinkedHashMap<>();

        DataFormatter df = new DataFormatter();
        for (Row row : ds) {
            Iterator<Cell> cellIterator = row.cellIterator();
            while (cellIterator.hasNext()) {
                Cell cell = cellIterator.next();
                String r = df.formatCellValue(cell);
                map.put(r,true);
            }
        }
        return map;
    }

}
