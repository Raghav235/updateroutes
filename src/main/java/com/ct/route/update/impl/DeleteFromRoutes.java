package com.ct.route.update.impl;

import com.ct.route.update.config.Config;
import com.ct.route.update.conn.CallApi;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class DeleteFromRoutes {

    public static void deleteFromRoutes(Sheet sheet, Config configuration, Map<String, Boolean> map) throws IOException {
        List<String> source = new ArrayList<>();
        List<String> dest = new ArrayList<>();
        int size = 0;
        DataFormatter dataFormatter = new DataFormatter();
        for (Row row : sheet) {
            Iterator<Cell> cellIterator = row.cellIterator();
            while (cellIterator.hasNext()) {
                Cell cell = cellIterator.next();
                String r = dataFormatter.formatCellValue(cell);
                cell = cellIterator.next();
                String r2 = dataFormatter.formatCellValue(cell);
                boolean flag = true;
                int i = 0;
                for (i = 0; i < source.size(); i++) {
                    if (source.get(i).equals(r2) && dest.get(i).equals(r)) {
                        flag = false;
                        break;
                    }
                }
                if (flag) {
                    source.add(r);
                    dest.add(r2);
                    size++;
                }
                break;

            }

        }
        System.out.println(source.size());
        for (int j = 0; j < source.size(); j++) {
            if(StringUtils.isBlank(source.get(j))){
                break;
            }
            if(map.containsKey(source.get(j)) && map.containsKey(dest.get(j))){
                System.out.println(source.get(j)+" "+dest.get(j));
               CallApi.callApi(source.get(j),dest.get(j),"delete",configuration,false);
            }
            else{
                CallApi.callApi(source.get(j),dest.get(j),"delete",configuration,true);
            }
        }

    }

}
