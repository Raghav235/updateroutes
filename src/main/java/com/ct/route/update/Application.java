package com.ct.route.update;

import com.ct.route.update.config.Config;
import com.ct.route.update.conn.GetConnection;
import com.ct.route.update.impl.AddToRoutes;
import com.ct.route.update.impl.DeleteFromRoutes;
import com.ct.route.update.impl.DomesticRoutesMap;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.yaml.snakeyaml.Yaml;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Map;

public class Application {


    public static void main(String[] args) throws IOException, InvalidFormatException {

        Config configuration;
        if( args.length != 1 ) {
            System.out.println( "Usage: <file.yml>" );
            return;
        }

        Yaml yaml = new Yaml();
        try( InputStream in = Files.newInputStream( Paths.get( args[ 0 ] ) ) ) {
            configuration = yaml.loadAs( in, Config.class );
            System.out.println( configuration.toString() );
        }

        Workbook workbook = GetConnection.getConnection(configuration);
        Sheet sheet0 = workbook.getSheetAt(0);
        Sheet sheet1 = workbook.getSheetAt(1);

        Map<String,Boolean> map = DomesticRoutesMap.getDomesticFlightsMap(configuration);
        addtoRoutes(sheet0,configuration,map);
        deleteFromRoutes(sheet1,configuration,map);


    }

    public static void addtoRoutes(Sheet sheet,Config config,Map<String,Boolean> map) throws IOException {
        AddToRoutes.addRoutes(sheet,config, map);
    }

    public static  void deleteFromRoutes(Sheet sheet,Config config,Map<String,Boolean> map) throws IOException {
        DeleteFromRoutes.deleteFromRoutes(sheet, config, map);
    }

}


